package com.example.springkotlindemo.entity

import org.springframework.data.annotation.Id
import java.time.LocalDateTime

/**
 * @author Xu Haidong
 * @date 2023/4/23 17:13
 */
class DemoUser {
    @Id
    var userId: Long? = null

    var deptId: Long? = null

    var userName: String? = null

    var nickName: String? = null

    var userType: String? = null

    var email: String? = null

    var phonenumber: String? = null

    var sex: String? = null

    var avatar: String? = null

    var password: String? = null

    var status: String? = null

    var delFlag: String? = null

    var loginIp: String? = null

    var loginDate: LocalDateTime? = null

    var createBy: String? = null

    var createTime: LocalDateTime? = null

    var updateBy: String? = null

    var updateTime: LocalDateTime? = null

    var remark: String? = null

    companion object {

        val tableName = "sys_user"
    }

}