package com.example.springkotlindemo.entity

import org.springframework.data.annotation.Id
import java.time.LocalDateTime
import java.util.*

/**
 * @author Xu Haidong
 * @date 2023/4/25 9:46
 */
class MyTest {

    @Id
    var id: Long? = null

    var name: String? = null

    var createTime: LocalDateTime? = null

    companion object {

        val tableName = "my_test"
    }
}