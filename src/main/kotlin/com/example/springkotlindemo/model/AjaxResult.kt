package com.example.springkotlindemo.model

import org.springframework.http.HttpStatus
import java.util.HashMap

/**
 * @author Xu Haidong
 * @date 2023/4/24 13:30
 */
class AjaxResult : HashMap<String, Any?> {
    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    constructor() {}

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     */
    constructor(code: Int, msg: String?) {
        super.put(CODE_TAG, code)
        super.put(MSG_TAG, msg)
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    constructor(code: Int, msg: String?, data: Any?) {
        super.put(CODE_TAG, code)
        super.put(MSG_TAG, msg)
        super.put(DATA_TAG, data)
    }

    /**
     * 方便链式调用
     *
     * @param key 键
     * @param value 值
     * @return 数据对象
     */
    override fun put(key: String, value: Any?): AjaxResult {
        super.put(key, value)
        return this
    }

    companion object {
        private const val serialVersionUID = 1L

        /** 状态码  */
        const val CODE_TAG = "code"

        /** 返回内容  */
        const val MSG_TAG = "msg"

        /** 数据对象  */
        const val DATA_TAG = "data"

        /**
         * 返回成功消息
         *
         * @param msg 返回内容
         * @param data 数据对象
         * @return 成功消息
         */
        fun success(
            code: Int = HttpStatus.OK.value(),
            msg: String = "操作成功",
            data: Any? = null
        ): AjaxResult {
            return AjaxResult(code, msg, data)
        }

        /**
         * 返回错误消息
         *
         * @param code 状态码
         * @param msg 返回内容
         * @return 警告消息
         */
        fun error(
            code: Int = HttpStatus.INTERNAL_SERVER_ERROR.value(),
            msg: String = "操作失败",
            data: Any? = null
        ): AjaxResult {
            return AjaxResult(code, msg, data)
        }
    }
}
